module nodep2p

go 1.17

require github.com/vmihailenco/msgpack v4.0.4+incompatible

require (
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/vmihailenco/msgpack/v5 v5.3.5 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	golang.org/x/net v0.0.0-20190603091049-60506f45cf65 // indirect
	google.golang.org/appengine v1.6.7 // indirect
)
