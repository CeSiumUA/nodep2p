package main

import (
	"fmt"
	"net"
	"strings"

	"github.com/vmihailenco/msgpack"
)

type Peer net.Conn

type NetworkCommand struct {
	Command string
	Payload []byte
}

type StartPeerConnectionCommand struct {
	Peers []string
}

var activePeers = []Peer{}

func main() {
	listener, err := net.Listen("tcp", "0.0.0.0:13768")
	if err != nil {
		fmt.Println(err)
	}
	defer listener.Close()
	fmt.Println("Hole punching server started!")

	for {
		fmt.Println("Listening for clients")
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("Error on connecting", err)
			continue
		}
		address := conn.RemoteAddr().String()
		localAddress := conn.LocalAddr().String()
		fmt.Println("Remote client connected ", address, " ", localAddress)

		if !Any(address) {
			activePeers = append(activePeers, conn)
			go DataProcessor(conn)
		}

		fmt.Println("Active peers count: ", len(activePeers))
	}
}

func DataProcessor(conn net.Conn) {
	defer conn.Close()
	for {
		input := make([]byte, 4096)
		n, err := conn.Read(input)
		if n == 0 || err != nil {
			fmt.Println("Read error", err)
			//activePeers = Filter(conn.RemoteAddr().String(), func(a, b string) bool {
			//	return a != b
			//})
			fmt.Println("Client disconnected and removed, ", conn.RemoteAddr().String())
			break
		}

		source := string(input[0:n])
		fmt.Println("Got command:", source, "from:", conn.RemoteAddr().String())
		if source == "getpeers" {
			filteredPeers := Filter(conn.RemoteAddr().String(), func(a, b string) bool {
				return a != b
			})
			addressesStrings := make([]string, len(filteredPeers))
			for indx, filteredPeer := range filteredPeers {
				addressesStrings[indx] = filteredPeer.RemoteAddr().String()
			}
			fmt.Println("Sending response:", addressesStrings)
			conn.Write([]byte(strings.Join(addressesStrings, ",")))
		}

		if source == "getselfaddress" {
			fmt.Println("Writing address response, ", conn.RemoteAddr().String())
			conn.Write([]byte(conn.RemoteAddr().String()))
		}

		if source == "startpunching" {
			fmt.Println("Started puncher")

			for _, peer := range activePeers {
				filteredPeers := Filter(peer.RemoteAddr().String(), func(a, b string) bool {
					return a != b
				})
				addressesStrings := make([]string, len(filteredPeers))
				for indx, filteredPeer := range filteredPeers {
					addressesStrings[indx] = filteredPeer.RemoteAddr().String()
				}
				command := StartPeerConnectionCommand{
					Peers: addressesStrings,
				}
				commandBytes := command.Serialize()
				networkCommand := NetworkCommand{
					Command: "startpunching",
					Payload: commandBytes,
				}
				networkCommandBytes := networkCommand.Serialize()
				peer.Write(networkCommandBytes)
			}
		}
	}
}

func Any(value string) bool {
	for _, b := range activePeers {
		if b.RemoteAddr().String() == value {
			return true
		}
	}
	return false
}

func Filter(value string, predicate func(a string, b string) bool) []Peer {
	filteredArray := []Peer{}
	for _, val := range activePeers {
		if predicate(val.RemoteAddr().String(), value) {
			filteredArray = append(filteredArray, val)
		}
	}
	return filteredArray
}

func (c *StartPeerConnectionCommand) Serialize() []byte {
	b, err := msgpack.Marshal(c)
	if err != nil {
		fmt.Println(err)
	}
	return b
}

func (c *NetworkCommand) Serialize() []byte {
	b, err := msgpack.Marshal(c)
	if err != nil {
		fmt.Println(err)
	}
	return b
}

func DeserializeStartPeerConnectionCommand(buffer []byte) *StartPeerConnectionCommand {
	command := StartPeerConnectionCommand{}
	err := msgpack.Unmarshal(buffer, &command)
	if err != nil {
		fmt.Println(err)
	}
	return &command
}

func DeserializeNetworkCommand(buffer []byte) *NetworkCommand {
	command := NetworkCommand{}
	err := msgpack.Unmarshal(buffer, &command)
	if err != nil {
		fmt.Println(err)
	}
	return &command
}
